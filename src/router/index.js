import { createRouter, createWebHashHistory } from 'vue-router'
import Login from '../views/Login.vue';
import SignUp from '../views/SignUp.vue';
import LogOut from '../views/LogOut.vue';

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: SignUp
  },
  {
    path: '/logout',
    name: 'LogOut',
    component: LogOut
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
